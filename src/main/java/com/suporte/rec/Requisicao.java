/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suporte.rec;

import com.suporte.enums.EnumStatusRequisicao;

public class Requisicao {
    private String tituloRequisicao;
    private String Descricao;
    private EnumStatusRequisicao status;

    public EnumStatusRequisicao getStatus() {
        return status;
    }

    public void setStatus(EnumStatusRequisicao status) {
        this.status = status;
    }

    public String getTituloRequisicao() {
        return tituloRequisicao;
    }

    public void setTituloRequisicao(String tituloRequisicao) {
        this.tituloRequisicao = tituloRequisicao;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }
  
}
