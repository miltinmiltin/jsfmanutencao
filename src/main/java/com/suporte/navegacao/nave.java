/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suporte.navegacao;

import java.io.Serializable;
import javax.inject.Named;

/**
 *
 * @author Hamilton
 */
@Named 
public class nave implements Serializable {
    
    public String pagina(){
        return "main?faces-redirect=true";
    }
    
    public String sobre(){
        return "sobre?faces-redirect=true";
    }
    
      public String Admin(){
        return "adminUser?faces-redirect=true";
    }
}
