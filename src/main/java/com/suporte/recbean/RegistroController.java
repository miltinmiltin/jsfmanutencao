/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suporte.recbean;

import com.suporte.enums.EnumStatusRequisicao;
import com.suporte.rec.Requisicao;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Hamilton
 */

@ManagedBean
@ApplicationScoped
public class RegistroController {
    private Requisicao registro = new Requisicao();
    private List<Requisicao> registros = new ArrayList<Requisicao>();
     
    public void adicionar(){        
        registro.setStatus(EnumStatusRequisicao.PENDENTE);
        this.registros.add(registro);
        System.out.println(registro.getStatus());
        this.registro = new Requisicao();
    }
    
//     public void adicionar(){
//        getRegistros().add(getRegistro());
//        this.setRegistro(new Requisicao());
//    }

    public Requisicao getRegistro() {
        return registro;
    }

    public void setRegistro(Requisicao registro) {
        this.registro = registro;
    }

    public List<Requisicao> getRegistros() {
        return registros;
    }

    public void setRegistros(List<Requisicao> registros) {
        this.registros = registros;
    }

     

    
}
